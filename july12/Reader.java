import java.util.Scanner;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileWriter;

public class Reader
{
	public static void main(String[] args) throws IOException
	{
		FileReader fr = new FileReader("input.txt");
		Scanner scanner = new Scanner(fr);
		
		FileWriter fw = new FileWriter("output.txt");
		PrintWriter output = new PrintWriter(fw);
		
		
		while (scanner.hasNext())
		{
			scanner.next();
			double type = scanner.nextDouble();
			scanner.next();
			double level = scanner.nextDouble();
			scanner.next();
			double attack = scanner.nextDouble();
			scanner.next();
			double defense = scanner.nextDouble();
			scanner.next();
			double base = scanner.nextDouble();
			scanner.next();
			double stab = scanner.nextDouble();
			scanner.next();
			double critical = scanner.nextDouble();
			scanner.next();
			double random = scanner.nextDouble();
			
			int damage = (int)((((2.0 * level + 10) / 250) * (attack/defense) * base + 2)
				* stab * type * critical * random);
				
			output.println(damage);
		}
		
		scanner.close();
		output.close();
	}
}