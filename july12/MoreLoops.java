import java.util.Scanner;

public class MoreLoops
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		String input;
		
		do
		{
			System.out.println("Welcome to the math tutor");
			input = scanner.next();
			
			if (input.equals("+"))
				System.out.println("addition");
			else if (input.equals("-"))
				System.out.println("subtraction");
			else if (input.equals("*"))
				System.out.println("multiplication");
			else if (input.equals("/"))
				System.out.println("division");
			else if (input.equals("q"))
			//continue makes the loop move on to the next iteration, skipping the rest
				continue;
			else
				System.out.println("Invalid input");
			
			//If continue was hit doStuff won't happen
			//doStuff();
			
		} while (!input.equals("q"));
	}
}