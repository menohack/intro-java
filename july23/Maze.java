public class Maze
{
	enum Step
	{
		RIGHT, LEFT, UP, DOWN
	}
	
	public static void main(String[] args)
	{
		final char[][] maze = { "xxxxxxxxxx".toCharArray(),
										"s xxx   xx".toCharArray(),
										"x   x x  e".toCharArray(),
										"xxx   xxxx".toCharArray(),
										"xxxxxxxxxx".toCharArray() };
										
		//This is what SolveMaze should produce
		Step[] answer = { Step.RIGHT, Step.DOWN, Step.RIGHT, Step.RIGHT, Step.DOWN,
								Step.RIGHT, Step.RIGHT, Step.UP, Step.UP, Step.RIGHT,
								Step.RIGHT, Step.DOWN, Step.RIGHT, Step.RIGHT };
								
		Step[] result = new Step[answer.length];
		SolveMaze(maze, result);
		
		if (CheckResult(answer, result))
			System.out.println("Correct!");
		else
			System.out.println("Wrong answer.");
			
		CheckValidMoves(maze);
	}
	
	/** Computes the steps, in order, to move from s to e, writing the result
			into steps. Note that maze[0][0] refers to the TOP LEFT. The x- and
			y-positions are accessed like this: maze[y][x]. x increases from
			left to right but y increases from top to bottom.
		@param maze The two-dimensional array of chars that represents the maze.
		@param steps The blank array of steps. Put your answer here.
	*/
	public static void SolveMaze(final char[][] maze, Step[] steps)
	{
		boolean found = false;
		int x = 0, y = 0;
		for (; y < maze.length && !found; y++)
			for (x = 0; x < maze[0].length && !found; x++)
				if (maze[y][x] == 's')
					found = true;
		x--;
		y--;
		
		int s = 0;
		Step previous = null;
		while (maze[y][x] != 'e')
		{
			if (ValidMove(maze, x+1, y) && previous != Step.LEFT)
			{
				steps[s++] = Step.RIGHT;
				x++;
			}
			else if (ValidMove(maze, x-1, y) && previous != Step.RIGHT)
			{
				steps[s++] = Step.LEFT;
				x--;
			}
			else if (ValidMove(maze, x, y+1) && previous != Step.UP)
			{
				steps[s++] = Step.DOWN;
				y++;
			}
			else if (ValidMove(maze, x, y-1) && previous != Step.DOWN)
			{
				steps[s++] = Step.UP;
				y--;
			}
			else
			{
				System.out.println("Uhhh...what?");
				return;
			}
			
			previous = steps[s-1];
		}
	}
	
	/** Checks whether a move is valid. A valid move should be within the bounds
			of the arrays and maze[y][x] should be ' ' or 'e'.
		@param maze The maze.
		@param x The x-position.
		@param y The y-position
		@return Returns true if the move is valid, false otherwise.
	*/
	public static boolean ValidMove(char[][] maze, int x, int y)
	{
		if (x < 0 || y < 0)
			return false;
		if (y >= maze.length || x >= maze[y].length)
			return false;
		if (maze[y][x] == 'x' || maze[y][x] == 's')
			return false;
		else if (maze[y][x] == ' ' || maze[y][x] == 'e')
			return true;
		else
		{
			System.out.println("Invalid character in maze");
			return false;
		}
	}
	
	/** Verifies the ValidMove function.
		@param maze The maze to check against.
	*/
	public static void CheckValidMoves(char[][] maze)
	{
		if (ValidMove(maze, -1, 0) || ValidMove(maze, 0, -1))
			System.out.println("Invalid negative position!");
		if (ValidMove(maze, 0, maze.length) || ValidMove(maze, maze[0].length, 0))
			System.out.println("Invalid positive position!");
		if (!ValidMove(maze, 9, 2))
			System.out.println("Move to e should be valid!");
		if (!ValidMove(maze, 3, 3))
			System.out.println("Move to blank should be valid!");
		if (ValidMove(maze, 0, 0))
			System.out.println("Move to x should be invalid!");
	}
	
	/** Verifies a result.
		@param answer The correct answer.
		@param result The result of SolveMaze.
		@return True of the answer and result match, false otherwise.
	*/
	public static boolean CheckResult(Step[] answer, Step[] result)
	{
		//If the number of steps don't match return false
		if (answer.length != result.length)
			return false;
			
		//If any step is wrong return false
		for (int i=0; i < answer.length; i++)
			if (answer[i] != result[i])
				return false;
		
		//Else return true
		return true;
	}
}