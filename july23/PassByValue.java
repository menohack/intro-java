import java.util.Scanner;

public class PassByValue
{
	public static void main(String[] args)
	{
		Scanner s = new Scanner("main");
		int t = 7;
		
		method(s, t);
		
		//Prints "main" because method() does not change this s reference
		System.out.println(s.next());
	}
	
	//changes the s variable ONLY IN THIS METHOD
	//This does not change where the original s points
	public static void method(Scanner s, int t)
	{
		s = new Scanner("method");
	}
}