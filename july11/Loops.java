import java.util.Scanner;

public class Loops
{
	public static void main(String[] args)
	{
		//i++ is equivalent to i = i + 1;
		
		//What is the difference between ++i and i++?
		//++i increments first
		//i++ uses the value then increments
		//i++ increments after the line is executed
	
		int i = 0;
		while (i < 4)
			System.out.println(i++);
			
		//The break keywords exits the surrounding loop
			
		//We don't need the curly braces because if is "a single statement"
			
		while (true)
		{
			if (i++ > 6)
			{
				break;
			}
		}
		
		//Completely equivalent to
		while (true)
			if (i++ > 6)
				break;
		
		
		System.out.println(i);
		
		Scanner scanner = new Scanner(System.in);
		
		int current = 0;
		
		//A do-while executes the block BEFORE testing, guaranteeing at least
		//one iteration of the loop
		do
		{
			System.out.println("Enter a number greater than 10");
			current = scanner.nextInt();
		}
		while (current > 10);
	
		for (i=0; i < 4; i++)
			System.out.println("Hello #" + i);	
			
		
		for (int x=0; x < 10; x++)
		{
			for (int y=0; y < 10; y++)
				System.out.print(y*10 + x + " ");
			System.out.println();
		}
	
		//This is valid. The intitializer and update are option, the condition is not.
		//for ( ; true; )
		//	;
		
		//You can have multiple initializers
		for (int a = 2, b = 6; a < 10 && b > 0; a++, b--)
			;
			
		//To call a method type it's name with ()
		method();
	}
	
	static void method()
	{
		System.out.println("Ran method");
	}
	
}