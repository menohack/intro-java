import java.util.Scanner;

//Note that lowercase scanner is OK, capital would conflict with java.util.Scanner
public class scanner
{
	public static void main(String[] arg)
	{
		String newString = new String();
		//Shorthand for new String
		String s = "Hello, World!";
		
		//Constructs a scanner that reads from the string s
		Scanner stringScanner = new Scanner(s);
		
		//Abandon ship!
		//if (something bad happens)
		//	return;

		
		//Prints "Hello,"
		System.out.println(stringScanner.next());
		
		//Prints 'e'
		System.out.println(s.charAt(1));
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter some text:");
		
		//Reads in and prints a whole line
		System.out.println(scanner.nextLine());
		
		System.out.println("Print your first name and your age");
		
		//We want to read in something like "James 23"
		String name = scanner.next();
		int age = scanner.nextInt();
		
		System.out.println("Hello " + name + ", you are " + age);
		
		//I want to print each word on a new line
		// \n means newline \t means tab
		System.out.println("\t\none\nline\nper\nword\\");
		
		//printf means print formatted
		//printf(formatString, ...)
		// ... means an unspecified number of variables
		//each variable matches the % in the format string
		//%s is string, %d is integer, %f is float
		System.out.printf("Hello my name is %s and I am %d years old\n", "James", 23);
		
		//Whitespace like newlines don't matter
		//scanner.next();scanner.next();
		
		//This prints 0 because (1/2) is integer division which results in 0
		//The integer 0 is then cast to a double 0.0
		//The .3 specifies 3 digits after the decimal point
		System.out.printf("%.3f\n", 2.0 * 4 * (1/2));
		
		//Now 1.0/2 is floating point division, yielding 0.5
		System.out.printf("%.3f\n", 2.0 * 4 * (1.0/2));
	}
}