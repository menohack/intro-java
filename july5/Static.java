import java.util.Scanner;
//import java.util.*;

public class Static
{
	int t;
	
	public static void main(String[] args)
	{
		//Scanner methods are non-static -- you need a scanner object
		Scanner s1 = new Scanner(System.in);
		Scanner s2 = new Scanner(System.in);
		
		//Here we use s2 as our object
		int exponent = s2.nextInt();
		
		
		//Math methods are static -- you use the Math class, not a Math object
		//Computes pi ^ exponent
		double p = Math.pow(Math.PI, exponent);
		
		System.out.println(p);
	}

}