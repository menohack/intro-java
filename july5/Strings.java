import java.util.Scanner;

public class Strings
{
	public static void main(String[] args)
	{
		String s = "Pikachui";
		
		//Looks for 'i' starting at the first character
		int index = s.indexOf('i');
		
		System.out.println(index);
		
		index = s.indexOf('i', 2);
		
		System.out.println(index);
		
		//substring makes a new string starting at the index 2
		System.out.println(s.substring(2));
		
		//Initialize to no string. This string does not exist.
		String nullString = null;
		
		//Initialize to the empty string. This string exists but is empty.
		String blankString = "";
		
		//Turns a string into an integer
		int i = Integer.parseInt("7");
		
		//You need the () around i+1 or it will print 71 instead of 8
		System.out.println("7 turned into an int + 1 = " + (i+1));
		
		//== is equals operator, which is different from = the gets operator
		//and different from equals()
		//int t = 7; -> int t gets 7
		
		//== compares the actual variable
		//== and != evaluate to true or false
		String s1 = "Hello";
		String s2 = s1;
		
		//s1 == s2 will be true because they point to the same memory
		//However, s2 == "Hello" will not be true
		
		String s3 = "Hello";
		String s4 = "Hello";
		
		//s3 == s4 is different from s3.equals(s4)
		//== compares where they point, which in this case is to different strings
		//equals() compares character by character, which in this case is true
		
		System.out.println("Input the word \"Type\" or \"type\"");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.next();
		
		if (input.equals("Type"))
			System.out.println("Correct with case!");
		else if (input.equals("type"))
			System.out.println("Correct without case!");
		else
			System.out.println("Wrong entirely");
	}
}