import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Canvas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Arrays;

/** YOU DO NOT NEED TO STUDY THIS CODE! GUI elements will NOT be on the final.
	However, the other classes in this folder demonstrate inheritance.
*/
public class Game extends JPanel implements ActionListener
{
	//Array of GameObjects
	//It contains all of the things in the world: Players, Enemies, Projectiles etc
	private GameObject[] gameObjects = new GameObject[3];
	
	private long lastTime = System.nanoTime();
	private final Timer timer = new Timer(16, this);
	BufferedImage bufferedImage = new BufferedImage(400, 400, BufferedImage.TYPE_INT_ARGB);

	public static void main(String[] args)
	{
		Game game = new Game();
		JFrame frame = new JFrame("Our Game");
		frame.getContentPane().add(game);
		frame.setSize(500,500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);
	}
	
	public Game()
	{
		//super("Our Game");
		
		Init();
		
		gameObjects[0] = new Player(300, 200);
		gameObjects[1] = new Player(100, 300);
		gameObjects[2] = new Enemy(200, 100);
		
		timer.start();
		
		GameObject max = findGreatest(gameObjects);
		System.out.println("The greatest x value is: " + max.x);
		
		GameObject[] array = gameObjects;
		
		//This sorts the array since every object implements Comparable
		Arrays.sort(gameObjects);
	}
	
	public static GameObject findGreatest(GameObject[] gs)
	{
		GameObject currentMax = gs[0];
		for (int i = 1; i < gs.length; i++)
			if (gs[i].compareTo(currentMax) > 0)
				currentMax = gs[i];
				
		return currentMax;
	}
	
	private void Init()
	{
		setSize(400,400);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setLayout(null);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		this.repaint();
	}
	
	public void paintComponent(Graphics g) {
		//Update the time
		long currentTime = System.nanoTime();
		long elapsed = currentTime - lastTime;
		lastTime = currentTime;
		double delta = elapsed/1000000000.0;
		
		//Clear
		g.setColor(Color.WHITE);
		g.fillRect(0,0,400,400);
		
		//Framerate
		//g.setColor(Color.BLACK);
		//g.drawString(""+gameObjects[0].y, 200, 100);
		
		for (int i=0; i < 3; i++)
		{
			//Update will call Player's Update for Players
			//and Enemy's Update for Enemies even though the
			//array is of type GameObject
			gameObjects[i].Update(delta);
			g.setColor(gameObjects[i].getColor());
			g.fillRect((int)gameObjects[i].x, (int)gameObjects[i].y, 40, 40);
		}
	}
}