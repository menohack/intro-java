import java.awt.Color;

public class Enemy extends GameObject
{

	public Enemy(float x, float y)
	{
		super(x, y);
	}
	
	public Color getColor()
	{
		return Color.RED;
	}
}