import java.awt.Color;

public class Player extends GameObject
{
	public Player(float x, float y)
	{
		super(x, y);
		yVelocity = -200;
	}
	
	public Color getColor()
	{
		return Color.BLUE;
	}
	
	public static final double GRAVITY = 100.0;
	public static final double BOUNCE_FACTOR = 0.4;
	
	public void Update(double deltaTime)
	{
		xVelocity = 10.0;
		yVelocity += deltaTime * GRAVITY;
	
		x += deltaTime * xVelocity;
		y += deltaTime * yVelocity;
		
		if (y > 360)
		{
			y = 360;
			yVelocity = -yVelocity * BOUNCE_FACTOR;
		}
	}
}