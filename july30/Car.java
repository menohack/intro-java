public class Car extends Vehicle implements EZPass
{
	public static void main(String[] args)
	{
		Vehicle honda = new Car("Honda", 20);
		honda.Drive();
		
		EZPass pass = (Car)honda;
		pass.PayToll();
	}
	
	public void Drive()
	{
		System.out.println("Driving a " + model + " at " + mpg);
	}
}