import java.awt.Color;

//Abstract base class!
public abstract class GameObject implements Comparable
{
	public double x, y;
	protected double xVelocity, yVelocity;
	private int guid;
	
	private static int idCount = 0;
	
	public GameObject(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.xVelocity = 0;
		this.yVelocity = 0;
		
		guid = nextGUID();
	}
	
	public static int nextGUID()
	{
		return idCount++;
	}
	
	public int getGUID()
	{
		return guid;
	}
	
	public int compareTo(Object o)
	{
		GameObject other = (GameObject)o;
		return (int)(this.x - other.x);
	}

	public void Update(double deltaTime)
	{
	}
	
	public abstract Color getColor();
}