   import java.util.Scanner;

/** The card class stores the suit and face of a playing card.
 *  Aces are high. Write the two constructors and the CompareTo method.
 *  You may want to have private helper methods to check suits and faces.
 */
   public class Card
   {
      private String suit;
      private int face;
   
      public static void main(String[] args)
      {
         Card c1 = new Card("Ace of Hearts");
      
         Card c2 = new Card(6, "Diamonds");
      
         if (c1.CompareTo(c2) > 0)
            System.out.println("c1 is greater than c2");
         else if (c1.CompareTo(c2) < 0)
            System.out.println("c1 is less than c2");
         else
            System.out.println("c1 is equal than c2");
      }
   	  
      public Card(String faceAndSuit)
      {
      }

      public Card(int face, String suit)
      {
			this.face = face;
			this.suit = suit;
      }
   	  
		//If you type c1.CompareTo(c2)
   	//then in that function
   	//c1 is called "this"
   	  
      public int CompareTo(Card otherz)
      {
        	if (this.face > otherz.face)
				return 1;
			else
				return 0;
      }
   	  
   	  //You could have also made a static method and called it like this:
   	  //CompareToStatic(c1, c2);
   	  
      static int CompareToStatic(Card a, Card b)
      {
        		//There is no "this" in a static method
				return 0;
      }
   	  
   }