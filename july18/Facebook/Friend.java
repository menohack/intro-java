package Facebook;

public class Friend
{
	//Start off as private and relax permissions if needed
	public String name;
	private String university;
	//No public, protected, or private means package-private
	//package-private means any class in the same package has access
	String friends = "";

	public static void main(String[] args)
	{
		String s = Integer.toHexString(-3);
	
		Friend mark = new Friend("Mark");
		mark.SetUniversity("Johns Hopkins University");
		
		Friend jim = new Friend("Jim", "Harvard University");
		Friend sally = new Friend("Sally", "Loyola University");
		mark.AddFriend(jim.GetName());
		mark.AddFriend(sally.GetName());
		
		//mark.PrintFriends();
		
		jim.AddFriend(mark.GetName());
		
		//jim.PrintFriends();
		
		//System.out.println(mark);
		//System.out.println(jim);
		
		Chat chat = new Chat(mark, jim);
		
		///Friend.SwapFriendData(mark, jim);
		
		System.out.println(mark);
		System.out.println(jim);
	}
	
	//Parameters can "shadow" fields
	//Meaning that name refers to the parameter and not the field
	public Friend(String n)
	{
		//This is generally bad practice
		//this.name = name;
		name = n;
	}
	
	public Friend(String n, String u)
	{
		name = n;
		university = u;
	}
	
	/** Adds a friend.
	 */
	public void AddFriend(String newFriend)
	{
		//Same as
		//friends = friends + newFriend;
		friends += " " + newFriend;
	}
	
	public void PrintFriends()
	{
		System.out.println(friends);
	}
	
	public String GetName()
	{
		return name;
	}
	
	public void SetName(String newName)
	{
		name = newName;
	}
	
	public void SetUniversity(String u)
	{
		university = u;
	}
	
	public String GetUniversity()
	{
		return university;
	}
	
	/** toString is defined automatically for every class but we
	 *  can redefine it.
	 */ 
	public String toString()
	{
		return name + " goes to " + university;
	}
	
	/** Swap the data of two friends.
	*/
	static void SwapFriendData(Friend a, Friend b)
	{
		String temp = a.name;
		a.name = b.name;
		b.name = temp;
		
		temp = a.university;
		a.university = b.university;
		b.university = temp;
		
		temp = a.friends;
		a.friends = b.friends;
		b.friends = temp;
	}
}