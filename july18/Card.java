import java.util.Scanner;

/** The card class stores the suit and face of a playing card.
 *  Aces are high. Write the two constructors and the CompareTo method.
 *  You may want to have private helper methods to check suits and faces.
 */
public class Card
{
	public static void main(String[] args)
	{
		Card c1 = new Card("Ace of Hearts");

		Card c2 = new Card(6, "Diamonds");

		if (c1.CompareTo(c2) > 0)
			System.out.println("c1 is greater than c2");
		else if (c1.CompareTo(c2) < 0)
			System.out.println("c1 is less than c2");
		else
			System.out.println("c1 is equal than c2");
	}
}
