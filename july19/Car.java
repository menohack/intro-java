public class Car
{
	//Static variables
	static int numWheels = 4;
	static int numSeats = 4;
	static String manufacturer = "Honda";
	static String model = "Accord";
	static Random rand;
	
	//Non-static
	Wheel wheel1, wheel2, wheel3, wheel4;
	Engine engine;
	SteeringWheel steering;
	Chassis chassis;

}