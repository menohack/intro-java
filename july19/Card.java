import java.util.Scanner;

/** The card class stores the suit and face of a playing card.
 *  Aces are high.
 */
public class Card
{
	public static void main(String[] args)
	{
		Card c1 = new Card("Ace of Hearts");

		Card c2 = new Card(6, "Diamonds");

		if (c1.CompareTo(c2) > 0)
			System.out.println("c1 is greater than c2");
		else if (c1.CompareTo(c2) < 0)
			System.out.println("c1 is less than c2");
		else
			System.out.println("c1 is equal than c2");
	}

	private String suit;
	private int face;

	public Card(String suitAndFace)
	{
		Scanner scanner = new Scanner(suitAndFace);
		String f = scanner.next();
		CheckFace(f);
			
		scanner.next();
		
		this.suit = scanner.next();
		CheckSuit();
	}

	public int CompareTo(Card other)
	{
		if (face > other.face)
			return 1;
		else if (face < other.face)
			return -1;
		else return 0;
	}

	public Card(int face, String suit)
	{
		this.face = face;
		if (face < 2 && face > 14)
			System.out.println("Invalid face");

		this.suit = suit;
		CheckSuit();
	}
		
	private void CheckFace(String f)
	{
		if (f.equalsIgnoreCase("Ace"))
			face = 14;
		else if (f.equalsIgnoreCase("Jack"))
			face = 11;
		else if (f.equalsIgnoreCase("Queen"))
			face = 12;
		else if (f.equalsIgnoreCase("King"))
			face = 13;
		else if (Integer.parseInt(f) > 1 && Integer.parseInt(f) < 11)
			face = Integer.parseInt(f);
		else
			System.out.println("Invalid face");
	}
	
	private void CheckSuit()	
	{
		if (!(suit.equalsIgnoreCase("Diamonds") || suit.equalsIgnoreCase("Spades") || suit.equalsIgnoreCase("Clubs") || suit.equalsIgnoreCase("Hearts")))
			System.out.println("Invalid suit");
	}
}
