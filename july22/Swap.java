import java.util.Scanner;

public class Swap
{
	public static void main(String[] args)
	{
		int a = 2;
		int b = 3;
		
		System.out.println("a is " + a + ", b is " + b);
		
		//It is impossible to write this method in Java
		//Swap does not modify a or b in main
		Swap(a, b);
		
		System.out.println("a is " + a + ", b is " + b);
		
		Scanner s = new Scanner("Hello, Entire World!");
		
		//Prints "Hello,"
		System.out.println(s.next());
		
		//Scans in "Entire" and discards it, modifying s
		Next(s);
		
		//Prints "World!"
		System.out.println(s.next());
	}
	
	//Java only has pass by reference for objects
	//But pass by value for primitives
	
	//a and b are COPIED as NEW VARIABLES, meaning that changing them in this
	//method DOES NOT change them in main
	public static void Swap(int a, int b)
	{
		int temp = a;
		a = b;
		b = temp;
	}
	
	//Objects are pass by REFERENCE, meaning that s REFERS to the variable
	//in main. If we change s by callign s.next(), the s in main is also changed
	public static void Next(Scanner s)
	{
		s.next();
	}
}