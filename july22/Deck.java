public class Deck
{
	//Fields
	private Card[] cards = new Card[NUM_CARDS];
	public static final int NUM_CARDS = 52;

	//Methods
	public static void main(String[] args)
	{
		Deck deck1 = new Deck();
		Deck deck2 = new Deck();
			
		System.out.println(deck1);
	}
	
	public Deck()
	{
		for (int i=0; i < NUM_CARDS; i++)
			cards[i] = new Card(2, "Hearts");
	}
	
	public String toString()
	{
		String string = "";
		for (int i=0; i < NUM_CARDS; i++)
			string += cards[i] + "\n";
			
		return string;
	}
}