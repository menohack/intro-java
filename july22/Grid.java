public class Grid
{
	public static void main(String[] args)
	{
		char[][] grid = new char[10][11];
		
		for (int i=0; i < grid.length; i++)
			for (int j=0; j < grid[i].length; j++)
				grid[i][j] = ' ';
		
		MakeBox(grid);
		Print(grid);
	}
	
	public static void MakeBox(char grid[][])
	{
		for (int i=0; i < grid.length; i++)
			for (int j=0; j < grid[i].length; j++)
				if (i == 0 || i == grid.length-1 || j == 0 || j == grid[i].length-1)
					grid[i][j] = 'x';
	}
	
	public static void Print(char[][] grid)
	{
		for (int i=0; i < grid.length; i++)
		{
			for (int j=0; j < grid[i].length; j++)
				System.out.print(grid[i][j] + " ");
			System.out.println();
		}
	}
}