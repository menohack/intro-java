public class Average
{
	public static void main(String[] args)
	{
		int data[] = { 2, 9, 4, 6, 3 };
		
		double result = Average(data);
		System.out.println("The average is: " + result);
	}
	
	public static double Average(int array[])
	{
		double sum = 0;
		for (int i=0; i < array.length; i++)
			sum += array[i];
		
		return sum/array.length;
	}
}