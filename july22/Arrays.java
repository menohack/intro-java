public class Arrays
{
	public static void main(String[] args)
	{
		//Initializer list, doesn't require new
		int values[] = { 0, 2, 10, 3 };
		
		int sum = 0;
		for ( int i=0; i < values.length; i++)
			sum += values[i];
			
		int third = values[2];
		//System.out.println("Third: " + third);
			
		//System.out.println("Sum: " + sum);
		
		//These cause ArrayIndexOutOfBoundsException, which crashes your program
		//System.out.println(values[-1]);
		//System.out.println(values[2000]);
		
		int data[] = new int[6];
		for (int i=0; i < data.length; i++)
			data[i] = i*2;
	}
}